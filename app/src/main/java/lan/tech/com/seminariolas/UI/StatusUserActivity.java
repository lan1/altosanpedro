package lan.tech.com.seminariolas.UI;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lan.tech.com.seminariolas.R;
import lan.tech.com.seminariolas.adapters.TransactionAdapter;
import lan.tech.com.seminariolas.model.Transaction;

public class StatusUserActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private TextView tvTotal;
    private int total;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private String user;
    private FirebaseUser current;
    private ChildEventListener childEventListener;
    private List<Transaction> items;
    private int admin;
    private SharedPreferences preferences;
    private NotificationManagerCompat notificationManager;
    private NotificationCompat.Builder mBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);
        Bundle bundle = getIntent().getExtras();
        Toolbar toolbar = findViewById(R.id.toolbar);
        total = 0;
        tvTotal = findViewById(R.id.tv_total);
        mAuth = FirebaseAuth.getInstance();
        current = mAuth.getCurrentUser();
        if (current == null) {
            if (preferences.getString("user", "").equals("")) {
                mAuth.signInWithEmailAndPassword(preferences.getString("user", ""), preferences.getString("pass", ""));
                current = mAuth.getCurrentUser();
            }
        }
        preferences = getSharedPreferences("my_preferences", Context.MODE_PRIVATE);
        setSupportActionBar(toolbar);
        if (bundle != null) {
            user = bundle.getString("user", "");
            admin = bundle.getInt("admin", 0);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(bundle.getString("name", ""));
            }
        }
        if (admin == 1) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        items = new ArrayList<Transaction>();
        mDatabase = FirebaseDatabase.getInstance().getReference("transaction/" + user);
        loadData();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_transaction);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new TransactionAdapter(items);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (admin == 0) {
            getMenuInflater().inflate(R.menu.admin, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_logout:
                logout();
                items.clear();
                return true;
            case android.R.id.home:
                finish();
                return true;
            case R.id.item_about:
                goToAbout();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void goToAbout() {
        Intent i = new Intent(this, AboutActivity.class);
        startActivity(i);
    }

    private void loadData() {
        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Transaction transaction = dataSnapshot.getValue(Transaction.class);
                items.add(transaction);
                Collections.sort(items);
                Collections.reverse(items);
                adapter.notifyDataSetChanged();
                total += transaction.getQuantity();
                tvTotal.setText(total + "");
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Transaction transaction = dataSnapshot.getValue(Transaction.class);
                List<Transaction> aux = new ArrayList<Transaction>();
                for (Transaction a : items) {
                    if (!a.getKey().equals(transaction.getKey())) {
                        aux.add(a);
                    }
                }
                items.clear();
                items.addAll(aux);
                Collections.sort(items);
                Collections.reverse(items);
                adapter.notifyDataSetChanged();
                total -= transaction.getQuantity();
                tvTotal.setText(total + "");

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mDatabase.addChildEventListener(childEventListener);
    }

    private void logout() {
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            mAuth.signOut();
        }
        SharedPreferences.Editor sp = preferences.edit();
        sp.clear();
        sp.apply();
        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(i);
        finish();
    }

}
