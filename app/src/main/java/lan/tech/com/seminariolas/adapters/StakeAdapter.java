package lan.tech.com.seminariolas.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import lan.tech.com.seminariolas.R;
import lan.tech.com.seminariolas.model.SumUser;

public class StakeAdapter extends RecyclerView.Adapter<StakeAdapter.StakeAdapterViewHolder> {
    private List<SumUser> items;

    public StakeAdapter(List<SumUser> lista) {
        items = lista;
    }

    @Override
    public StakeAdapter.StakeAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.stake_users, parent, false);
        return new StakeAdapterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(StakeAdapter.StakeAdapterViewHolder holder, int position) {
        holder.name.setText(items.get(position).getName());
        holder.total.setText(items.get(position).getTotal()+"");
        holder.user.setText(items.get(position).getUser());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class StakeAdapterViewHolder extends RecyclerView.ViewHolder {
        TextView user;
        TextView name;
        TextView total;

        public StakeAdapterViewHolder(View v) {
            super(v);
            user = v.findViewById(R.id.tv_card_user);
            name = v.findViewById(R.id.tv_card_name);
            total = v.findViewById(R.id.tv_card_total);
        }
    }
}
