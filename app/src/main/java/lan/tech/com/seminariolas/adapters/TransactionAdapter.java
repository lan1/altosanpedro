package lan.tech.com.seminariolas.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import lan.tech.com.seminariolas.R;
import lan.tech.com.seminariolas.model.Transaction;

/**
 * Created by lan on 02-12-17.
 */

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder> {
    private List<Transaction> items;

    public static class TransactionViewHolder extends RecyclerView.ViewHolder {
        public TextView detail;
        public TextView mount;
        public TextView date;

        public TransactionViewHolder(View v) {
            super(v);
            detail = v.findViewById(R.id.tv_card_detail);
            mount = v.findViewById(R.id.tv_card_mount);
            date = v.findViewById(R.id.tv_card_date);
        }
    }

    public TransactionAdapter(List<Transaction> item) {
        this.items = item;
        Collections.sort(items);
        Collections.reverse(items);
    }

    @Override
    public TransactionAdapter.TransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_card, parent, false);
        return new TransactionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TransactionAdapter.TransactionViewHolder holder, int position) {
        holder.detail.setText(items.get(position).getDetail());
        holder.mount.setText(items.get(position).getQuantity() + "");
        if (items.get(position).getQuantity() < 1) {
            holder.mount.setTextColor(Color.parseColor("#DC143C"));
        } else {
            holder.mount.setTextColor(Color.parseColor("#2f8182"));
        }
        long time = items.get(position).getTimestamp();
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }
        Date date = new Date(time);
        holder.date.setText(new SimpleDateFormat("dd/MM/yyyy").format(date));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
