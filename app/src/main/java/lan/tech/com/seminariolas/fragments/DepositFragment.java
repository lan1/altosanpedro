package lan.tech.com.seminariolas.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import lan.tech.com.seminariolas.R;
import lan.tech.com.seminariolas.model.Transaction;
import lan.tech.com.seminariolas.model.User;

public class DepositFragment extends Fragment {
    private Spinner spinner;
    private Transaction transaction;
    private User user;
    private Button deposit;
    private EditText userDeposit;
    private EditText mountDeposit;
    private FirebaseDatabase database;
    private DatabaseReference reference;
    private DatabaseReference root;
    private DatabaseReference usuario;

    public DepositFragment() {
        // Required empty public constructor
    }

    public static DepositFragment newInstance() {
        DepositFragment fragment = new DepositFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_deposit, container, false);
        userDeposit = rootView.findViewById(R.id.et_user_deposit);
        mountDeposit = rootView.findViewById(R.id.et_mount_deposit);
        database = FirebaseDatabase.getInstance();
        transaction = new Transaction();
        user = new User();
        deposit = rootView.findViewById(R.id.btn_deposit);
        deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userDeposit.getText().toString().equals("") || mountDeposit.getText().toString().equals("")) {
                    if (userDeposit.getText().toString().equals(""))
                        userDeposit.setError("Campo Vacio");
                    if (mountDeposit.getText().toString().equals(""))
                        mountDeposit.setError("Campo vacio");
                } else {
                    final String current = userDeposit.getText().toString();
                    usuario = database.getReference("users");
                    usuario.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.child(current).exists()) {
                                user = dataSnapshot.child(current).getValue(User.class);
                                openConfigDialog();
                            } else {
                                Toast.makeText(rootView.getContext(), "No existe el usuario", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }
            }
        });
        return rootView;
    }

    public void openConfigDialog() {
        reference = database.getReference("transaction/" + userDeposit.getText().toString());
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
        alertBuilder.setTitle(R.string.confirm_deposit);
        alertBuilder.setView(R.layout.content_deposit);
        alertBuilder.setPositiveButton(R.string.confirm, null);
        alertBuilder.setNegativeButton(R.string.cancel, null);
        final AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();

        Button buttonConfirm = (Button) alertDialog.findViewById(android.R.id.button1);
        TextView tvUser = (TextView) alertDialog.findViewById(R.id.tv_user);
        TextView tvName = alertDialog.findViewById(R.id.tv_name);
        TextView tvMount = (TextView) alertDialog.findViewById(R.id.tv_mount);
        tvUser.setText(userDeposit.getText().toString());
        tvMount.setText(mountDeposit.getText().toString());
        tvName.setText(user.getName());

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deposit();
                alertDialog.dismiss();
            }
        });

    }

    private void deposit() {
        String key = reference.push().getKey();
        String admin = FirebaseAuth.getInstance().getCurrentUser().getEmail();
        String[] temp = admin.split("@");
        admin = temp[0];
        transaction.setAdmin(admin);
        int d = Integer.parseInt(mountDeposit.getText().toString());
        transaction.setQuantity(d);
        transaction.setKey(key);
        Date date = new Date();
        Long time = date.getTime();
        transaction.setTimestamp(time);
        transaction.setDetail("Recibiste seminariolas por tu esfuerzo esta semana!");
        user.setTotal(user.getTotal() + d);
        final String usuario = userDeposit.getText().toString();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/users/" + usuario + "/total", user.getTotal());
        childUpdates.put("/ward/" + usuario + "/total", user.getTotal());
        childUpdates.put("/transaction/" + usuario + "/" + key, transaction.toMap());
        root = database.getReference("/");
        root.updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getContext(), "Deposito exitoso a " + usuario, Toast.LENGTH_SHORT).show();
            }
        });
        userDeposit.setText("");
        mountDeposit.setText("");
    }

}
