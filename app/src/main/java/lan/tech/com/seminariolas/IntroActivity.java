package lan.tech.com.seminariolas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import lan.tech.com.seminariolas.UI.AdminActivity;
import lan.tech.com.seminariolas.UI.LoginActivity;
import lan.tech.com.seminariolas.UI.StatusUserActivity;
import lan.tech.com.seminariolas.utils.TypeUser;

public class IntroActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private ImageView ivLogo;
    private boolean isLogged;
    private FirebaseDatabase database;
    private DatabaseReference users;
    private ValueEventListener mlistener;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        ivLogo = (ImageView) findViewById(R.id.ivLogo);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        users = database.getReference("users");
        preferences = getSharedPreferences("my_preferences", Context.MODE_PRIVATE);
        updateUI(mAuth.getCurrentUser());
        Animation animacion = AnimationUtils.loadAnimation(
                getApplicationContext(), R.anim.intro);
        ivLogo.startAnimation(animacion);

        animacion.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (hasNetworkConnection()) {
                    if (isLogged) {
                        mlistener = new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                String current = mAuth.getCurrentUser().getEmail();
                                String[] cu = current.split("@");
                                current = cu[0];
                                if (dataSnapshot.child(current).exists()) {
                                    int userType = dataSnapshot.child(current).child("type").getValue(Integer.class);
                                    if (userType == TypeUser.ADMIN) {
                                        Intent intent = new Intent(IntroActivity.this, AdminActivity.class);
                                        startActivity(intent);
                                        finish();
                                    } else if (userType == TypeUser.STUDENT) {
                                        Intent i = new Intent(IntroActivity.this, StatusUserActivity.class);
                                        i.putExtra("user", current);
                                        i.putExtra("name", dataSnapshot.child(current).child("name").getValue(String.class));
                                        i.putExtra("admin", 0);
                                        startActivity(i);
                                        finish();
                                    } else {
                                        Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                } else {
                                    Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        };
                        users.addListenerForSingleValueEvent(mlistener);
                    } else {
                        Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "No tienes conexion a internet, conectate y reinicia la app", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            isLogged = true;
        } else {
            String adm = preferences.getString("admin", "");
            String us = preferences.getString("user", "");
            String pass = preferences.getString("pass", "");
            if (!us.equals("")) {
                mAuth.signInWithEmailAndPassword(us, pass);
                isLogged = true;
            } else if (!adm.equals("")) {
                mAuth.signInWithEmailAndPassword(adm, pass);
                isLogged = true;
            } else {
                isLogged = false;
            }

        }
    }

    //    private void createNotificationChannel() {
    //        // Create the NotificationChannel, but only on API 26+ because
    //        // the NotificationChannel class is new and not in the support library
    //        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
    //            CharSequence name = getString(R.string.channel_name);
    //            String description = getString(R.string.channel_description);
    //            int importance = NotificationManager.IMPORTANCE_DEFAULT;
    //            NotificationChannel channel = new NotificationChannel(TypeUser.CHANNEL_ID, name, importance);
    //            channel.setDescription(description);
    //            // Register the channel with the system; you can't change the importance
    //            // or other notification behaviors after this
    //            NotificationManager notificationManager = getSystemService(NotificationManager.class);
    //            notificationManager.createNotificationChannel(channel);
    //        }
//    }

    private boolean hasNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}
