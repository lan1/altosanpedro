package lan.tech.com.seminariolas.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import lan.tech.com.seminariolas.R;
import lan.tech.com.seminariolas.model.SumUser;
import lan.tech.com.seminariolas.model.User;
import lan.tech.com.seminariolas.utils.TypeUser;
import lan.tech.com.seminariolas.utils.WardUtils;

public class CreateUserFragment extends Fragment {
    private EditText cUser;
    private EditText cName;
    private EditText cPassword;
    private Button create;
    private DatabaseReference root;
    private User nuevo;
    private SumUser sumNuevo;
    private FirebaseAuth auth;
    private SharedPreferences preferences;

    public CreateUserFragment() {
        // Required empty public constructor
    }


    public static CreateUserFragment newInstance() {
        CreateUserFragment fragment = new CreateUserFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_create_user, container, false);
        // Inflate the layout for this fragment
        auth = FirebaseAuth.getInstance();
        cUser = rootView.findViewById(R.id.et_create_user);
        cName = rootView.findViewById(R.id.et_create_name);
        cPassword = rootView.findViewById(R.id.et_create_password);
        create = rootView.findViewById(R.id.btn_registrar);
        preferences = this.getActivity().getSharedPreferences("my_preferences", Context.MODE_PRIVATE);
        final String[] estacas = {WardUtils.MIRAFLORES, WardUtils.ASANPEDRO, WardUtils.COPACABANA, WardUtils.CALACOTO, WardUtils.CONSTITUCION};
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cUser.getText().toString().equals("") || cPassword.getText().toString().equals("") || cName.getText().toString().equals("")) {
                    if (cUser.getText().toString().equals("")) {
                        cUser.setError("Campo vacio");
                    }
                    if (cPassword.getText().toString().equals("")) {
                        cPassword.setError("Campo vacio");
                    }
                    if (cName.getText().toString().equals("")) {
                        cName.setError("Campo vacio");
                    }
                } else {
                    registerNewUser();
                }
            }
        });


        return rootView;
    }

    private void registerNewUser() {
        final String newUser = cUser.getText().toString();
        String newName = cName.getText().toString();
        final int newPassword = Integer.parseInt(cPassword.getText().toString());
        nuevo = new User();
        nuevo.setName(newName);
        nuevo.setPassword(newPassword);
        nuevo.setTotal(0);
        nuevo.setType(TypeUser.STUDENT);
        sumNuevo = new SumUser();
        sumNuevo.setName(newName);
        sumNuevo.setUser(newUser);
        sumNuevo.setTotal(0);
        root = FirebaseDatabase.getInstance().getReference("/");
        final String admin = preferences.getString("admin", "");
        final String pass = preferences.getString("pass", "");
        auth.createUserWithEmailAndPassword(newUser + "@gmail.com", newPassword + "")
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Map<String, Object> childUpdates = new HashMap<>();
                            childUpdates.put("/users/" + newUser, nuevo.toMap());
                            childUpdates.put("/ward/"+ newUser, sumNuevo.toMap());
                            root.updateChildren(childUpdates);
                            Toast.makeText(getContext(), "Se registro exitosamente a " + newUser, Toast.LENGTH_SHORT).show();
                            cName.setText("");
                            cPassword.setText("");
                            cUser.setText("");
                            auth.signOut();
                            auth.signInWithEmailAndPassword(admin, pass);

                        }
                    }
                });
    }
}
