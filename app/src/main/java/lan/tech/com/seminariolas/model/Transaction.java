package lan.tech.com.seminariolas.model;

import android.support.annotation.NonNull;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lan on 30-11-17.
 */

public class Transaction implements Comparable<Transaction> {
    private int quantity;
    private String admin;
    private String key;
    private Long timestamp;
    private String detail;

    public Transaction() {
    }

    public Transaction(int quantity, String admin, String key, Long timestamp, String detail) {
        this.quantity = quantity;
        this.admin = admin;
        this.key = key;
        this.timestamp = timestamp;
        this.detail = detail;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    @Override
    public int compareTo(@NonNull Transaction transaction) {
        return getTimestamp().compareTo(transaction.getTimestamp());
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("key", getKey());
        result.put("admin", getAdmin());
        result.put("quantity", getQuantity());
        result.put("timestamp", getTimestamp());
        result.put("detail", getDetail());
        return result;
    }
}
