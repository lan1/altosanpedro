package lan.tech.com.seminariolas.utils;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.support.design.widget.TabLayout;

import lan.tech.com.seminariolas.R;

/**
 * Created by lan on 10-12-17.
 */

public class Tabs {
    private TabLayout tabLayout;
    private Activity context;

    public Tabs(Activity context, TabLayout tabLayout) {
        this.tabLayout = tabLayout;
        this.context = context;
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setSelectedTabIndicatorColor(context.getResources().getColor(R.color.colorAccent));
        tabLayout.setSelectedTabIndicatorHeight(10);
        setupTabIcons();
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_account_balance);
        tabLayout.getTabAt(0).getIcon().setColorFilter(context.getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_add_shopping_cart);
        tabLayout.getTabAt(1).getIcon().setColorFilter(context.getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_IN);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(context.getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tabLayout.getTabAt(0).getIcon().clearColorFilter();
                tab.getIcon().setColorFilter(context.getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
