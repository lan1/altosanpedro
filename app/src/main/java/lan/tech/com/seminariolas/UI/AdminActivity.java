package lan.tech.com.seminariolas.UI;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import lan.tech.com.seminariolas.R;
import lan.tech.com.seminariolas.adapters.SectionsPagerAdapter;
import lan.tech.com.seminariolas.fragments.AboutFragment;
import lan.tech.com.seminariolas.fragments.CreateUserFragment;
import lan.tech.com.seminariolas.fragments.DepositFragment;
import lan.tech.com.seminariolas.fragments.SellFragment;
import lan.tech.com.seminariolas.fragments.StakeStatusFragment;

public class AdminActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private FirebaseAuth mAuth;
    private SectionsPagerAdapter sectionsPagerAdapter;
    private NavigationView navigationView;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        mAuth = FirebaseAuth.getInstance();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        preferences = getSharedPreferences("my_preferences", Context.MODE_PRIVATE);
        if (findViewById(R.id.fragment_container) != null) {
            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }
            getSupportActionBar().setTitle(R.string.to_deposit);
            // Create a new Fragment to be placed in the activity layout
            DepositFragment firstFragment = new DepositFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            firstFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, firstFragment).commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setCheckedItem(0);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (id) {
            case R.id.nav_vender:
                transaction.replace(R.id.fragment_container, new SellFragment());
                transaction.addToBackStack(null);
                transaction.commit();
                getSupportActionBar().setTitle(R.string.to_sell);

                break;
            case R.id.nav_depositar:
                transaction.replace(R.id.fragment_container, new DepositFragment());
                transaction.addToBackStack(null);
                transaction.commit();
                getSupportActionBar().setTitle(R.string.to_deposit);
                break;
            case R.id.nav_esp:
                goToStake();
                getSupportActionBar().setTitle("Seminaristas");
                break;
            case R.id.nav_logout:
                logout();
                break;
            case R.id.nav_about:
                goToAbout();
                break;
            case R.id.nav_crear:
                goToCreate();
                break;

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void goToCreate() {
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, new CreateUserFragment());
        transaction.addToBackStack(null);
        transaction.commit();
        getSupportActionBar().setTitle("Crear nuevo usuario");
    }


    private void goToStake() {
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        StakeStatusFragment estaca = new StakeStatusFragment();
        transaction.replace(R.id.fragment_container, estaca);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void goToAbout() {
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, new AboutFragment());
        transaction.addToBackStack(null);
        getSupportActionBar().setTitle(R.string.about);
        transaction.commit();
    }

    private void logout() {
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            mAuth.signOut();
        }
        SharedPreferences.Editor sp = preferences.edit();
        sp.clear();
        sp.apply();
        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAuth.getCurrentUser() != null) {
            String admin = preferences.getString("admin", "");
            String pass = preferences.getString("pass", "");
            mAuth.signInWithEmailAndPassword(admin, pass);
        }

    }

}
