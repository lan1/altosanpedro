package lan.tech.com.seminariolas.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import lan.tech.com.seminariolas.fragments.DepositFragment;
import lan.tech.com.seminariolas.fragments.SellFragment;

/**
 * Created by lan on 10-12-17.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = DepositFragment.newInstance();
                break;
            case 1:
                fragment = SellFragment.newInstance();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String name = null;
        if (position == 0)
            name = "Depositar";
        if (position == 1)
            name = "Vender";
        return name;
    }
}