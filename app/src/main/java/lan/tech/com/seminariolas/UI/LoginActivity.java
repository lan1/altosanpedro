package lan.tech.com.seminariolas.UI;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import lan.tech.com.seminariolas.R;
import lan.tech.com.seminariolas.utils.TypeUser;

public class LoginActivity extends AppCompatActivity {
    private Button btnLogIn;
    private EditText etUser;
    private EditText etPassword;
    private TextInputLayout textInputLayout;
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private DatabaseReference users;
    private ValueEventListener mlistener;
    private SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        users = database.getReference("users");
        textInputLayout = findViewById(R.id.text);
        btnLogIn = findViewById(R.id.btn_ingresar);
        etUser = findViewById(R.id.et_user);
        etPassword = findViewById(R.id.et_password);
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    textInputLayout.setPasswordVisibilityToggleEnabled(true);
                } else {
                    textInputLayout.setPasswordVisibilityToggleEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
//        etPassword.setText("123456");
        preferences = getSharedPreferences("my_preferences", Context.MODE_PRIVATE);
        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etUser.getText().toString().equals("") || etPassword.getText().toString().equals("")) {
                    if (etUser.getText().toString().equals("")) {
                        etUser.setError("Campo vacio");
                    }
                    if (etPassword.getText().toString().equals("")) {
                        etPassword.setError("Campo vacio");
                    }
                } else {
                    if (hasNetworkConnection()) {
                        final String admin = etUser.getText().toString() + "@gmail.com";
                        final String pass = etPassword.getText().toString();
                        mAuth.signInWithEmailAndPassword(admin, pass).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    mlistener = new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            String current = etUser.getText().toString();
                                            String[] cu = current.split("@");
                                            current = cu[0];
                                            if (dataSnapshot.child(current).exists()) {
                                                int userType = dataSnapshot.child(current).child("type").getValue(Integer.class);
                                                if (userType == TypeUser.ADMIN) {
                                                    saveName(admin, pass);
                                                    Intent intent = new Intent(LoginActivity.this, AdminActivity.class);
                                                    startActivity(intent);
                                                    finish();
                                                } else {
                                                    saveUser(admin, pass);
                                                    Intent i = new Intent(LoginActivity.this, StatusUserActivity.class);
                                                    i.putExtra("user", current);
                                                    i.putExtra("name", dataSnapshot.child(current).child("name").getValue(String.class));
                                                    i.putExtra("admin", 0);
                                                    startActivity(i);
                                                    finish();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    };
                                    users.addListenerForSingleValueEvent(mlistener);

                                } else {
                                    Toast.makeText(LoginActivity.this, "Usuario o contrasenia incorrecta", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                    } else {
                        Toast.makeText(LoginActivity.this, "No tienes conexion a internet", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void saveUser(String user, String pass) {
        SharedPreferences.Editor sp = preferences.edit();
        sp.putString("user", user);
        sp.putString("pass", pass);
        sp.apply();
    }

    private void saveName(String admin, String pass) {
        SharedPreferences.Editor sp = preferences.edit();
        sp.putString("admin", admin);
        sp.putString("pass", pass);
        sp.apply();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mAuth != null) {
            mAuth.signOut();
        }

    }

    public void hideKeyboard(@Nullable View view) {
        if (view == null) {
            view = this.getCurrentFocus();
        }
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private boolean hasNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}
