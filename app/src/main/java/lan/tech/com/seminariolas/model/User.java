package lan.tech.com.seminariolas.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lan on 30-11-17.
 */
@IgnoreExtraProperties
public class User {
    private String name;
    private String stake;
    private int password;
    private int type;
    private int total;

    public User() {
    }

    public User(String name, String stake, int password, int type, int total) {
        this.name = name;
        this.stake = stake;
        this.password = password;
        this.type = type;
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStake() {
        return stake;
    }

    public void setStake(String stake) {
        this.stake = stake;
    }

    public int getPassword() {
        return password;
    }

    public void setPassword(int password) {
        this.password = password;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("name", getName());
        result.put("stake", getStake());
        result.put("password", getPassword());
        result.put("type", getType());
        result.put("total", getTotal());
        return result;
    }
}
