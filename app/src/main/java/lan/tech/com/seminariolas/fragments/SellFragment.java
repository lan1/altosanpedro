package lan.tech.com.seminariolas.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import lan.tech.com.seminariolas.R;
import lan.tech.com.seminariolas.model.Transaction;
import lan.tech.com.seminariolas.model.User;

public class SellFragment extends Fragment {
    private EditText etUser;
    private TextInputEditText etDetail;
    private TextInputEditText etPrice;
    private Button btnToSell;
    private DatabaseReference users;
    private DatabaseReference root;
    private User client;
    private DatabaseReference transaction;
    private ValueEventListener valueEventListener;

    public SellFragment() {
    }

    public static SellFragment newInstance() {
        SellFragment fragment = new SellFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_sell, container, false);
        etUser = rootView.findViewById(R.id.et_user_sell);
        etDetail = rootView.findViewById(R.id.et_detail_sell);
        etPrice = rootView.findViewById(R.id.et_price_sell);
        btnToSell = rootView.findViewById(R.id.btn_to_sell);
        btnToSell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String user = etUser.getText().toString();
                String detail = etDetail.getText().toString();
                String price = etPrice.getText().toString();
                if (user.equals("") || detail.equals("") || price.equals("")) {
                    if (user.equals(""))
                        etUser.setError("Campo vacio");
                    if (detail.equals(""))
                        etDetail.setError("Campo vacio");
                    if (price.equals(""))
                        etPrice.setError("Campo vacio");
                } else {
                    users = FirebaseDatabase.getInstance().getReference("users");
                    valueEventListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.child(user).exists()) {
                                client = dataSnapshot.child(user).getValue(User.class);
                                int costo = Integer.parseInt(etPrice.getText().toString());
                                if (client.getTotal() < costo) {
                                    Toast.makeText(rootView.getContext(), "El usuario no tiene el monto suficiente para la compra", Toast.LENGTH_SHORT).show();
                                } else {
                                    openConfigDialog();
                                }

                            } else {
                                Toast.makeText(rootView.getContext(), "No existe el usuario", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    };
                    users.addListenerForSingleValueEvent(valueEventListener);
                }
            }
        });
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (users != null) {
            users.removeEventListener(valueEventListener);
        }

    }

    public void openConfigDialog() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
        alertBuilder.setTitle(R.string.confirm_deposit);
        alertBuilder.setView(R.layout.content_sell);
        alertBuilder.setPositiveButton(R.string.confirm, null);
        alertBuilder.setNegativeButton(R.string.cancel, null);
        final AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();

        Button buttonConfirm = (Button) alertDialog.findViewById(android.R.id.button1);
        TextView tvName = (TextView) alertDialog.findViewById(R.id.tv_name);
        TextView tvDetail = alertDialog.findViewById(R.id.tv_detail);
        TextView tvUser = (TextView) alertDialog.findViewById(R.id.tv_user);
        TextView tvMount = (TextView) alertDialog.findViewById(R.id.tv_mount);
        tvName.setText(client.getName());
        tvDetail.setText(etDetail.getText().toString());
        tvUser.setText(etUser.getText().toString());
        tvMount.setText(etPrice.getText().toString());
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sell();
                alertDialog.dismiss();
            }
        });
    }

    private void sell() {
        final String user = etUser.getText().toString();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        transaction = database.getReference("transaction/" + etUser.getText().toString());
        String key = transaction.push().getKey();
        Transaction nuevo = new Transaction();
        nuevo.setKey(key);
        nuevo.setDetail(etDetail.getText().toString());
        String admin = FirebaseAuth.getInstance().getCurrentUser().getEmail();
        String[] h = admin.split("@");
        admin = h[0];
        nuevo.setAdmin(admin);
        Date date = new Date();
        long time = date.getTime();
        nuevo.setTimestamp(time);
        int mount = Integer.parseInt(etPrice.getText().toString());
        mount *= (-1);
        nuevo.setQuantity(mount);
        root = database.getReference("/");
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/users/" + user + "/total", client.getTotal() + mount);
        childUpdates.put("/transaction/" + user + "/" + key, nuevo);
        childUpdates.put("/ward/" + user + "/total", client.getTotal() + mount);
        root.updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getContext(), "Venta exitosa a " + user, Toast.LENGTH_SHORT).show();

            }
        });
        etUser.setText("");
        etPrice.setText("");
        etDetail.setText("");
    }
}
